"use strict";

// Initialisation
const moment = require("moment-timezone"); // library for timezone logic
const TelegramBot = require('node-telegram-bot-api');
const puppeteer = require('puppeteer.js');
const igCmd = require('./libs/instagramcommands.js');
const tgCmd = require('./libs/telegramcommands.js');
const sqlCmd = require('.\\libs\\sqlcommands.js');
const config = require('.\\libs\\config.js');

const token = ''; // <-- Unique Telegram Bot token
const chat_id = "";

const bot = new TelegramBot(
	"API_TOKEN_HERE", // bot token from BotFather
  { polling: true }
 );

const admin = {
	igAccount:"@starspullingmyhair",
	tgUsername:"@starspullingmyhair",
	tgUserId:"",
};

let users = {};

moment.tz.setDefault("America/Tijuana"); // Set PST as default timezone

/** 
 * Main thread of execution.
 * Section of code which is responsible for running the puppeteer stuff.
 * Also starts bot stuff and keeps running it.
 * Starts event listeners to do this
 */

/** Main thread of execution */
(async function run() {
  // connect to the database
  // start a browser, ready to open tabs when needed
  // start a telegram bot
  // start listening for messages on the group
  // When someone posts a message, check that they already commented on the last 10.
  // start listening for direct messsages
  // trigger actions when certain events happen

  // Connects to the db
  let connection = await mysql.createConnection(config.sqlConnectionParameters);
  
  await connection.connect(async function(err) {
    if (err) {
      console.error('error connecting: ' + err.stack);
      return;
    }

    console.log('connected as id: ' + connection.threadId);
  });

  // Starts a browser
  const browser = await puppeteer.launch({headless:false, slowmo:100});
  // probably need to add a hello announcement

  
  telegramCommands.listenForMessages(); //  listens for players, parses for photos


  // probably need to start some event listeners to find messages, which will
  // trigger warning checks, etc.

  // need to add some logic to print the rules whenever a warning is issued


  const page = await browser.newPage();

})();

/** section of code responsible for running  */
