CREATE TABLE `telegram_users`
(
  `id` int NOT NULL AUTO_INCREMENT,
  `telegram_username` varchar(255),
  `warnings` tinyint
  PRIMARY KEY(`id`);
);

CREATE TABLE `instagram_users`
(
  `id` int NOT NULL AUTO_INCREMENT,
  `instagram_username` varchar(255),
  `warnings` tinyint
    PRIMARY KEY(`id`);
);

CREATE TABLE `moderators`
(
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255)
  PRIMARY KEY(`id`);
);

CREATE TABLE `whitelist`
(
  `id` int NOT NULL AUTO_INCREMENT,
  `instagram_username` varchar(255)
  PRIMARY KEY(`id`);
);

