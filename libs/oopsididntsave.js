// Selector to catch the element containing username for all comments in the comment list.
let commentTitleSelector #react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul > ul:nth-child(n) > div > li > div > div > div.C4VMK > h3 > a

// Need to add a method for scrolling the comments section. Probably borrow from instapy.
// Seems like it starts at the bottom of the comments list, so I need to scroll up?
// When there's lots of comments, I need to click a plus icon to load more.


// Collect Titles:
document.querySelectorAll('body > div._2dDPU.vCf6V > div.zZYga > div > article > div.eo2As > div.EtaWk > ul > ul:nth-child(n) > div > li > div > div.C7I1f > div.C4VMK > h3 > a')
// Verified that this selector gets a nodelist of all comments ^^^

// Scroll Comments:
// Looks like all the circle links aren't the same. So, if i can't find a circle, then I should assume that the list is loaded. This would even apply to all of them.
document.querySelector('body > div._2dDPU.vCf6V > div.zZYga > div > article > div.eo2As > div.EtaWk > ul > li > div > button > span');


// Collect photo owner:
document.querySelector('#react-root > section > main > div > div > article > header > div.o-MQd.z8cbW > div.PQo_0.RqtMr > div.e1e1d > h2 > a')


// Grab the load more comments button
#react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul > li > div > button

/**
 * Gets DOM element for the comments section.
 * Scrolls to the bottom of the comments section.
 */
async function loadComments() {

// selector for the comments element:
    let commentsSection = page.$('#react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul');
    
    // scrolls to the bottom.
    commentsSection.scrollTop = commentsSection.scrollHeight
}