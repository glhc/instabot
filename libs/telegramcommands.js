'use strict';

let config = require('config.js');

/**
 * todo:
 * write privateUserDialog(), for user dms
 * write adminDialog(), for admin dms
 * write findPlayerIgUsername()
 * write checkPlayerCommentedPhotos()
 */

// Section for Direct Message ability

/**
 * Listens for messages. Starts logic for different situations
 * @listens node-telegram-bot-api:message
 * @returns {object} messsage - the message
 */
exports.listenForMessages = bot.on('message', async function(message) {
  let instagramPhotoLink = "";

  /**
   * todo:
   * if there's an instagram photo link and the error checks pass, put it
   * into the sliding array of photo links
   */

  // when there's a message, check if it's a private message or a group one.
  if (message.chat.type === "private") {
    if (message.from.id === admin.tgid) {
      adminDialog(); //write me
    } else {
      privateUserDialog(); //write me
    }
  } else if (message.chat.type === "group" || "supergroup") {
    let instagramPhotoLink = await parseMessageForPhoto(message);

    /** 
     * if there's an instagram photo in the group message, check that
     * the player has met prerequisites
     */
    if (instagramPhotoLink) {
      await checkPostPrerequisites();
    }
  }
  
  // if there's a igphoto, check that the user follows admin on instagram.
});

/**
 * Checks that the poster meets prerequisites by:
 * 1. Checking that user is not banned
 * 2. Checking that user followed admin
 * 3. Checking that user commented on necessary photos
 * @param {*} post - Object representing post
 */
exports.checkPostPrerequisites = async function checkPostPrerequisites(post) {
  let player = {}; // object with player info
  player.tgid = await post.from.id;
  player.igusername = await igCmd.findPlayerIgUsername(); // needs arg
  
  // Error logic for bans
  if (sqlCmd.isTgUserBanned(player.tgid)) {
    throw new Error('Telegram ID is banned ', player.tgid);
  } else if (sqlCmd.isIgUserBanned(player.igUsername)) {
    throw new Error('Instagram user is banned:', player.igUsername);
  } else if (igCmd.checkPlayerFollowedAdmin) { // Check that user followed
    throw new Error('Instagram user did not follow admin:', player/igUsername);
  } else if (igCmd.checkPlayerCommentedPhotos(player.igUsername)) {
    return true;
  } else {
    return false;
  }

}

/**
 * Parses for instagram link in message using regex.
 * @returns {string} findings - contains photo url it exists, or a falsy if not.
 */
exports.parseMessageForPhoto = async function parseMessageForPhoto(message) {

  // Regex to find ig photo link
  let igPhotoRegex = /(?<instagramUrl>https:\/\/www\.instagram\.com\/p\/\w*)/

  // Match text
  let results = await message.text.match(igPhotoRegex);
  let findings = false;

  // If there is a photo link, set finding to instagram link.
  if (results) {
    findings = await results.groups.instagramUrl;
  }

  // Return instagram photo link if there is one.
  return findings;
}

exports.announce = async function announce() {
  let text = "Hello! welcome to the game." 
  let chatID = sql.

  bot.sendMessage(chatId, text)
} 

exports.botStart = bot.onText(/\/start/, (msg, match) => {
	// 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message
  let commandsString = 'Hi there! please find below a list of commands:';
  
  for (let i in commands) {
    commandsString += "\n" + config.commands[i];
  }

  bot.sendMessage(commandsString);
}); 

exports.botHelp = bot.onText(/\/help/, (msg, match) => {
	// 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message
}); 

 
// Command: Output administrator details
exports.botAdmin = bot.onText(/\/admin/, (msg, match) => {
	// 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message
  bot.sendMessage("The admin is: " + config.adminCreds.tgUsername);
}); 

// Command: Check own warnings
exports.botMyWarns = bot.onText(/\/mywarns/, (msg, match) => {
  let user = msg.from.id;
  let warns = accounts.user.warnings; 

	bot.sendMessage("You have " + warns + "/5 warnings."

	);

});

// Admin command: Warn a user
exports.botWarn = bot.onText(/\/warn/, (msg, match) => {
	// admin check
	if (msg.from.id === admin.tgUserId) {
		// get tgUsername string of the warned
		let stringIndex = msg.text.indexOf(" ") + 1; // get index of string
		let warned = msg.text.slice(msg.text[stringIndex]); // get tgUsername

		// Warn or ban them
		user.warnings++;
		
		if (warnedList.warned === 5) {
			bot.kickChatMember(chat_id, user_id);
	}};
});