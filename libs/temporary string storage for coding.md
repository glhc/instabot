# useful strings i need to put in code

## Instagram

### Query Selector for section holding igusername of owner in a photo link

```
#react-root > section > main > div > div > article > header > div.o-MQd.z8cbW > div.PQo_0.RqtMr > div.e1e1d > h2 > a
```

### Regex to parse username from html section holding photo owner username
```
(?:title="(?<igusername>\w*)")

result.groups.igusername should be checked here
```

### selector for section made by photo commenter 
```
#react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul > ul:nth-child(7) > div > li > div > div > div.C4VMK > h3 > a
```
```
#react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul > ul:nth-child(4) > div > li > div > div > div.C4VMK > h3 > a
```

It seems that the ul:nth-child will change depending on the number of the post.