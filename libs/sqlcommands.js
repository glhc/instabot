/** @module libs/mysqlqueries.js */
// Uses mysql npm package


/**
 * Checks to see if  results were returned from a MySQL query 
 * @param results - Results returns from query to check 
 * @returns {boolean} - True if there are results, false if not.
 */
function checkForResults (results) {
  let findings = false;

  if (results) {
    findings = true;
  }

  return findings;
}

/** 
 * Check for error in a MySQL query and nothing else
 * @const
 * @param possibleError - possible error item returned by MySql
 * @throws possibleError 
 */
const errorcheck = function(possibleError) {
  if (possibleError) {
    throw possibleError;
  }
}

/**
 * Get the warnings of a user.
 * @param {string} telegramUser - The telegram username to retreive warnings
 * for.
 * @returns {Number} - number of warnings out of 3
 */
exports.getTelegramWarnings = function getTelegramWarnings(telegramUser) {
  let answer = await connection.query(
    'SELECT `warnings` FROM `telegram_users` WHERE `telegram_username` = ?',
    [telegramUser], async function (error, results, fields) {
      if (error) throw error;
      console.log('User: ' + telegramUser + 'has: ' + results + '/5 warnings.');
    }
  )
}

/**
 * Get the warnings of a user.
 * @param {string} instagramUser - The telegram username to retreive warnings
 * for.
 * @returns {Number} - number of warnings out of max 5
 */
exports.getInstagramWarnings = function getInstagramWarnings(instagramUser) {
  connection.query(
    'SELECT `warnings` FROM `instagram_users` WHERE `instagram_username` = ?',
    [instagramUser], async function (error, results, fields) {
      if (error) throw error;
      console.log('User: ' + instagramUser + 'has: ' + results + '/5 warnings.');
    
    }
  )
}

/**
 * Gives a warning to an Instagram user
 * @throws error if too many warnings
 * @returns true if successful
 */
exports.giveWarningInstagramUser = async function 
  giveWarningInstagramUser (username) {
  
  let currentWarnings = await sqlCommands.getInstagramWarnings(username);
  if (currentWarnings >= 5) {
    throw new Error('User has reached limit. Warnings cannot be increased.');
  }

  await connection.query(
    'UPDATE `instagram_users` SET `warnings` = ? WHERE `instagram_username` = ?',
    [instagramUser], async function (error, results, fields) {
      if (error) throw error;
      console.log('User: ' + instagramUser + 'has: ' + results + '/3 warnings.');
    }
  );

  return true;
}

/**
 * Gives a warning to a Telegram User
 * @throws error if too many warnings
 * @returns true if successful
 */
exports.giveWarningTelegramUser = async function 
  giveWarningTelegramUser(username) {
  
  let currentWarnings = await sqlCommands.getTelegramWarnings(username);
  if (currentWarnings >= 5) {
    throw new Error('User has reached limit. Warnings cannot be increased.');
  }

  await connection.query(
    'UPDATE `telegram_users` SET `warnings` = ? WHERE `telegram_username` = ?',
    [telegram], async function (error, results, fields) {
      if (error) throw error;
      console.log('User: ' + telegramgramUser + 'has: ' + results + '/3 warnings.');
    }
  );

  return true;
}

exports.getTotalWhitelist = async function getTotalWhitelist() {
  connection.query('SELECT instagram_username FROM whitelist',

    function (error, results, fields) {
      if (error) throw error;
      if (checkForResults(results)) {
        return results;
      } else {
        throw error;
      }
  });
}  

/**
 * Checks if username is in whitelist
 * @returns boolean
 */
exports.checkWhitelistForUser = async function checkWhitelistForUser(username) {
  let string =
    'SELECT instagram_username FROM whitelist WHERE instagram_username' +
    connection.escape(username);
  
  connection.query(string, function (error, results, fields) {
    if (error) throw error;

    return checkForResults(results);
  });
}  

/**
 * Adds user to whitelist
 */
exports.addUserToWhitelist = async function addUserToWhitelist(igUsername) {
  let user = {intagram_username: igUsername};

  connection.query('INSERT INTO whitelist SET ?', user,
    function (error, results, fields) {
      if (error) throw error;
      console.log('Adding User: ' + igUsername + '. query to db: ' + query.sql);
    
    }
  );
}  