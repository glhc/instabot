/**
 * @module
 */

"use strict";


/** API token goes here */
exports.botApiToken = "";

/** mySQL connection parameters */
exports.sqlConnectionParameters = {
  host: 'localhost',
  user: 'sa',
  password: 'secret',
  database: 'instabotdb'
}