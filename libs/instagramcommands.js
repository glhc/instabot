/**
 * Commands for working with instagram
 * @module 'libs/instagramcommands.js'
 */

/**
 * todo:
 * write checkPlayerCommentedPhotos
 */

const puppeteer = require('puppeteer');

/**
 * Checks that the poster meets prerequisites by:
 * Checking that user followed admin
 * Checking that user commented on necessary photos
 * Check user is not banned
 * @param {string} photo - Link to poster's instagram photo
 */
exports.processPhotoLink = async function processPhotoLink(photo) {
  let user = await getPhotoOwnerUsername(photo)
  await checkUserFollowedAdmin(user);
  await checkPlayerCommentedPhotos(user);
  await 
}

/**
 * Checks that the user commented on the x most recent photos according to
 * The global config.dx setting.
 * @param {string} playerIg - Player's instagram username
 * @returns {boolean} - True if player commented on the most recent photos
 */
exports.checkPlayerCommentedPhotos =
    async function checkPlayerCommentedPhotos(playerIg) {
      for (let i = )

    
    };

/**
 * Creates a new Chromium tab.
 * @param {*} browser - puppeteer browser instance
 * @returns {*} page - a new Chromium tab 
 */
exports.createNewTab = async function createNewTab(browser) {
  const page = await browser.newPage

  return page;
}

/** Opens instagram photo
 * @param {string} profileLink - link to player's instagram photo
 * @param {*} page - page object to load instagram photo on.
 * @returns {boolean} - true if success, false if failure.
 */
exports.loadPage = async function loadPage(profileLink, page) {	
	await page.goto(profileLink)
    .catch(console.log(err));
  
};

/**
 * Given an Instagram photo page, loads all the comments for that page.
 * @param  {*} page - the pre-loaded Instagram photo page.
 * @throws Will throw an error if the comments section cannot be found.
 * @returns {boolean} - true if successful.
 */
exports.loadComments = async function loadComments(page) {

  // Finds comments element.
  let commentsSection = page.$('#react-root > section > ' + 
    'main > div > div > article > div.eo2As > div.EtaWk > ul');


  // Looks for whether it can load more comments. Null if not.
  let commentsPlusSign = await page.$('#react-root > section > main > div > ' + 
    'div > article > div.eo2As > div.EtaWk > ul > li > div > button');


  // scrolls to the bottom, clicking all the plus icons to load the lists.
  while (commentsPlusSign) {
    await commentsPlusSign.click();
    await page.waitForNavigation();

    commentsSection.scrollTop = 0; // scroll up
    
    // look for another plus sign
    commentsPlusSign = await page.$('#react-root > section > main > div > ' + 
      'div > article > div.eo2As > div.EtaWk > ul > li > div > button');
  }

  return true;
}


/**
 * Given a fully scrolled page, collects an array of all commenters
 * on that photo.
 * @param {*} page - The instagram photo link.
 * @returns {Set} igUsernames - A set of the instagram usernames of commenters.
 */
exports.getCommenterUsernameList = async function getCommenterUsernameList (page) {
  let igUsernames = new Set();

  // returns array of element handles pointing to comments
  let nodeList = await page.$$('#react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul > ul:nth-child(n) > div > li > div > div > div.C4VMK > h3 > a');

  nodeList.forEach(async function (item) {
    if (!igUsernames.has(item.title)) {
      igUsernames.add(item.title);
    }
  });

  return igUsernames;
}

/**
 * Collects the instagram username of the person who posted the photo
 * @param {*} page - the instagram photo page.
 * @returns {string} username - The photo poster's instagram username.
 */
exports.findPlayerIgUsername = async function findPlayerIgUsername (page) {
  let posterElement = await page.$('#react-root > section > main > div > ' + 
    'div > article > header > div.o-MQd.z8cbW > div.PQo_0.RqtMr > ' + 
    'div.e1e1d > h2 > a');
  
  let username = await posterElement.title;

  if (username !== typeof String) {
    throw new Error("username is not a string.");
  }

  return username;
}

exports.checkPlayerFollowedAdmin = async function checkPlayerFollowedAdmin(igusername) {
  page = await browser.newPage();
  await page.goto('instagram.com/' + igusername);

  // as long as the user isn't private, check that the player followed the admin
  if (!checkUserIsPrivate(page)) {
    // This requires the observer to be logged in to check an account's
    // followers
  }


}

/**
 * Checks the instagram username is private
 * @returns {boolean} - true if private, false if not
 */
exports.checkUserIsPrivate = async function checkUserIsPrivate(userpage) {
  let element  = await userpage.$(
      "#react-root > section > main > div > div.Nd_Rl._2z6nI > article > div > div > h2"
  )

  if (element.textContent) {
    return true;
  } else {
    return false;
  }

}
